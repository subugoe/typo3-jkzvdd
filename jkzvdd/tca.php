<?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

$TCA["tx_jkzvdd_mets"] = array (
	"ctrl" => $TCA["tx_jkzvdd_mets"]["ctrl"],
	"interface" => array (
		"showRecordFieldList" => "hidden"
	),
	"feInterface" => $TCA["tx_jkzvdd_mets"]["feInterface"],
	"columns" => array (
		'hidden' => array (		
			'exclude' => 1,
			'label'   => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config'  => array (
				'type'    => 'check',
				'default' => '0'
			)
		),
        "metsfile" => Array (        
            "exclude" => 1,        
            "label" => "LLL:EXT:jkzvdd/locallang_db.xml:tx_jkzvdd_mets.metsfile",        
            "config" => Array (
                "type" => "group",
                "internal_type" => "file",
                "allowed" => "",    
                "disallowed" => "php,php3",    
                "max_size" => 1000,    
                "uploadfolder" => "uploads/tx_jkzvdd",
                "size" => 1,    
                "minitems" => 0,
                "maxitems" => 1,
            )
        ),
        "collectionid" => Array (        
            "exclude" => 1,        
            "label" => "LLL:EXT:jkzvdd/locallang_db.xml:tx_jkzvdd_mets.collectionid",        
            "config" => Array (
                "type" => "input",    
                "size" => "48",    
                "eval" => "required",
            )
        ),
        "collectionname" => Array (        
            "exclude" => 1,        
            "label" => "LLL:EXT:jkzvdd/locallang_db.xml:tx_jkzvdd_mets.collectionname",        
            "config" => Array (
                "type" => "input",    
                "size" => "48",    
                "eval" => "required",
            )
        ),
        "uploaddate" => Array (        
            "exclude" => 1,        
            "label" => "LLL:EXT:jkzvdd/locallang_db.xml:tx_jkzvdd_mets.uploaddate",        
            "config" => Array (
                "type"     => "input",
                "size"     => "12",
                "max"      => "20",
                "eval"     => "datetime",
                "checkbox" => "0",
                "default"  => "0"
            )
        ),
        "emailuploader" => Array (        
            "exclude" => 1,        
            "label" => "LLL:EXT:jkzvdd/locallang_db.xml:tx_jkzvdd_mets.emailuploader",        
            "config" => Array (
                "type" => "input",    
                "size" => "48",    
                "eval" => "required",
            )
        )
    ),
    "types" => array (
        "0" => array("showitem" => ";;;;1-1-1, hidden;;1, metsfile, collectionid, collectionname, uploaddate, emailuploader")
    ),
    "palettes" => array (
        "1" => array("showitem" => "")
    )
);

$TCA["tx_jkzvdd_oai"] = array (
    "ctrl" => $TCA["tx_jkzvdd_oai"]["ctrl"],
    "interface" => array (
        "showRecordFieldList" => "hidden"
    ),
    "feInterface" => $TCA["tx_jkzvdd_oai"]["feInterface"],
    "columns" => array (
        'hidden' => array (        
            'exclude' => 1,
            'label'   => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
            'config'  => array (
                'type'    => 'check',
                'default' => '0'
            )
        ),
        "oaiurl" => Array (        
            "exclude" => 1,        
            "label" => "LLL:EXT:jkzvdd/locallang_db.xml:tx_jkzvdd_oai.oaiurl",        
            "config" => Array (
                "type" => "input",    
                "size" => "48",    
                "eval" => "required",
            )
        ),
        "collectionid" => Array (        
            "exclude" => 1,        
            "label" => "LLL:EXT:jkzvdd/locallang_db.xml:tx_jkzvdd_oai.collectionid",        
            "config" => Array (
                "type" => "input",    
                "size" => "48",    
                "eval" => "required",
            )
        ),
        "collectionname" => Array (        
            "exclude" => 1,        
            "label" => "LLL:EXT:jkzvdd/locallang_db.xml:tx_jkzvdd_oai.collectionname",        
            "config" => Array (
                "type" => "input",    
                "size" => "48",    
                "eval" => "required",
            )
        ),
        "entrydate" => Array (        
            "exclude" => 1,        
            "label" => "LLL:EXT:jkzvdd/locallang_db.xml:tx_jkzvdd_oai.entrydate",        
            "config" => Array (
                "type"     => "input",
                "size"     => "12",
                "max"      => "20",
                "eval"     => "datetime",
                "checkbox" => "0",
                "default"  => "0"
            )
        ),
        "emailentry" => Array (        
            "exclude" => 1,        
            "label" => "LLL:EXT:jkzvdd/locallang_db.xml:tx_jkzvdd_oai.emailentry",        
            "config" => Array (
                "type" => "input",    
                "size" => "48",    
                "eval" => "required",
            )
        ),
        "lastscan" => Array (        
            "exclude" => 1,        
            "label" => "LLL:EXT:jkzvdd/locallang_db.xml:tx_jkzvdd_oai.lastscan",        
            "config" => Array (
                "type"     => "input",
                "size"     => "12",
                "max"      => "20",
                "eval"     => "datetime",
                "checkbox" => "0",
                "default"  => "0"
            )
        ),
        "scanperiod" => Array (        
            "exclude" => 1,        
            "label" => "LLL:EXT:jkzvdd/locallang_db.xml:tx_jkzvdd_oai.scanperiod",        
            "config" => Array (
                "type" => "select",
                "items" => Array (
                    Array("LLL:EXT:jkzvdd/locallang_db.xml:tx_jkzvdd_oai.scanperiod.I.0", "86400"),
                    Array("LLL:EXT:jkzvdd/locallang_db.xml:tx_jkzvdd_oai.scanperiod.I.1", "86400"),
                    Array("LLL:EXT:jkzvdd/locallang_db.xml:tx_jkzvdd_oai.scanperiod.I.2", "604800"),
                    Array("LLL:EXT:jkzvdd/locallang_db.xml:tx_jkzvdd_oai.scanperiod.I.3", "2592000"),
                    Array("LLL:EXT:jkzvdd/locallang_db.xml:tx_jkzvdd_oai.scanperiod.I.4", "31536000"),
                ),
                "size" => 5,    
                "maxitems" => 1,
            )
        )
    ),
    "types" => array (
        "0" => array("showitem" => ";;;;1-1-1, hidden;;1, oaiurl, collectionid, collectionname, entrydate, emailentry, lastscan, scanperiod")
    ),
    "palettes" => array (
        "1" => array("showitem" => "")
    )
);

?>
