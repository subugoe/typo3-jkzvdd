<?php

/*
 * ***************************************************************
 *   Copyright notice
 * 
 *   (c) 2013 Niedersächsische Staats- und Universitätsbibliothek Göttingen
 *   Jochen Kothe (kothe@sub.uni-goettingen.de, jk@profi-php.de)
 *   All rights reserved
 * 
 *   This script free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   The GNU General Public License can be found at
 *   http://www.gnu.org/copyleft/gpl.html.
 * 
 *   This script is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   This copyright notice MUST APPEAR in all copies of the script!
 * ***************************************************************
 */

class tx_jkzvdd_oaigetmissing extends tx_scheduler_Task {

    var $extKey = 'jkzvdd';
    var $arrConf = array();
    var $logfile = '';
    
    
    public function execute() {
        set_time_limit(0);
        $this->arrConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['jkzvdd']);

        for($i=0; $i<count($this->selectedCollections); $i++) {
            if(!trim($this->selectedCollections[$i])) {
                continue;
            }
            $this->logfile = $this->arrConf['zvddrepository'].'oai_provider/'.trim($this->selectedCollections[$i]).'/logs/oaigetmissing.log';
            file_put_contents($this->logfile, date('Y-m-d H:i:s', time()) . ' Start oaigetmissing: ' . $this->selectedCollections[$i] . "\n", FILE_APPEND);

            $oaiUrl = $this->getOaiUrl($this->selectedCollections[$i]);
            file_put_contents($this->logfile, 'oai url:' . $oaiUrl . "\n", FILE_APPEND);

            $arrOaiIds = $this->getOaiIds($this->selectedCollections[$i]);
            
            if($oaiUrl) {
                $skip = 0;
                $fetch = 0;
                foreach($arrOaiIds as $oaiId) {
                    //check index for oai id
                    $solr = file_get_contents($this->arrConf['solrPhpsUrl'] . '&q=id_oai_s:"'.urlencode(trim($oaiId)).'"');
                    $arrSolr = unserialize($solr);
                    if (!$arrSolr['response']['numFound'] || !trim($arrSolr['response']['docs'][0]['dc'][0]) ) {
                        $fetch++;
                        $oaixml = $this->getOaiXml($oaiUrl.'?verb=GetRecord&metadataPrefix=mets&identifier='.urlencode(trim($oaiId)));
                        if($oaixml) {
                            file_put_contents($this->logfile, 'Fetch: ' . $oaiId . "\n", FILE_APPEND);
                            file_put_contents($this->arrConf['zvddrepository'].'oai_provider/'.trim($this->selectedCollections[$i]).'/oai/'.$this->id2name($oaiId).'.xml', $oaixml);
                        } else {
                            file_put_contents($this->logfile, 'Fetch error: ' . $oaiId . "\n", FILE_APPEND);
                            file_put_contents($this->arrConf['zvddrepository'].'oai_provider/'.trim($this->selectedCollections[$i]).'/oai/'.$this->id2name($oaiId).'.xml_error', '');
                        }
                    } else {
                        file_put_contents($this->logfile, 'Solr: ' . $arrSolr['response']['numFound'] . ' / DC: ' . $arrSolr['response']['docs'][0]['dc'][0] . ' / PID: ' . $arrSolr['response']['docs'][0]['pid'] . ' / OAI: ' . $arrSolr['response']['docs'][0]['id_oai_s'][0] . "\n", FILE_APPEND);
                        $skip++;
                        file_put_contents($this->logfile, 'Skip: ' . $oaiId . "\n", FILE_APPEND);                        
                    }
                }
                file_put_contents($this->logfile, 'Skipped: ' . $skip . ' ids / Fetch: ' . $fetch . ' ids' . "\n", FILE_APPEND);                        
            }
        }
        return true;
    }

    public function getAdditionalInformation() {
        return implode(', ', $this->selectedCollections);
    }

    /**
     * Get all OAI Identifiers from oai provider (collectionid)
     *
     * @return array oai ids
     */
    protected function getOaiIds($collectionid) {
        $res = $GLOBALS['TYPO3_DB']->exec_SELECTquery('*', 'tx_jkzvdd_oai', 'collectionid="' . mysql_real_escape_string($collectionid) . '" and not hidden and not deleted');
        if ($GLOBALS['TYPO3_DB']->sql_num_rows($res)) {
            $arr = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res);
            $listIdUrl = str_replace('ListRecords', 'ListIdentifiers', $arr['oaiurl']);
      
            $arrURL = parse_url($listIdUrl);
            if($arrURL['port']) {
                $resumptionTokenUrl = $arrURL['scheme'].'://'.$arrURL['host'].':'.$arrURL['port'].$arrURL['path'];
            } else {
                $resumptionTokenUrl = $arrURL['scheme'].'://'.$arrURL['host'].$arrURL['path'];
            }

            $oaixml = $this->getOaiXml($listIdUrl);
            $arrOaiIds = array();
            while($oaixml) {
                $arrTmp = $this->parseXml($oaixml);
                $arrOaiIds = array_merge($arrOaiIds, $arrTmp['ids']);
                if($arrTmp['resumptionToken']) {
                    $oaixml = $this->getOaiXml($resumptionTokenUrl . '?verb=ListIdentifiers&resumptionToken=' . urlencode(trim($arrTmp['resumptionToken'])));
                } else {
                    $oaixml = false;
                }
            }

            file_put_contents($this->logfile, 'Found: ' . count($arrOaiIds) . ' oai ids' . "\n", FILE_APPEND);
            return $arrOaiIds;
        }
        return false;
    }

    /**
     * Get xml answer from oai url
     * Example url : http://gdz.sub.uni-goettingen.de/oai2/?verb=ListIdentifiers&metadataPrefix=mets&set=vd18.digital
     * Example url : http://gdz.sub.uni-goettingen.de/oai2/?verb=ListIdentifiers&resumptionToken=oai_028c1d02fdf8c26cc2b335ee202111bb
     * @return string oai xml
     */
    protected function getOaiXml($oaiurl) {
        $repeat = 5;
        $count = 0;
        $xml = '';
        while (!$xml) {
            // break after $repeat trials
            if ($count > $repeat) {
                break;
            }
            $count++;
            $opts = array(
                'http' => array(
                    'method' => "GET",
                    'header' => "user-agent: ZVDD OAI2 Harvest\r\n"
                )
            );
            $context = stream_context_create($opts);
            $xml = file_get_contents(trim($oaiurl), false, $context);
            if (!$xml) {
                sleep($count * 3);
            } else {
                file_put_contents($this->logfile, 'Got xml from: ' . trim($oaiurl) . "\n", FILE_APPEND);
                return $xml;
            }
        }
        return false;
    }

    /**
     * Parse oai xml 
     *
     * @return array ('resumtionToken', array oai ids)
     */
    protected function parseXml($xml) {
        $dom = new DOMDocument('1.0', 'UTF-8');
        $test = $dom->loadXML($xml);

        if($test) {
            //get error
            $errorNode = $dom->getElementsByTagName('error');
            if ($errorNode->length) {
                return false;
            }

            //get resumptionToken
            $node = $dom->getElementsByTagName('resumptionToken');
            if ($node->length && trim($node->item(0)->nodeValue)) {
                file_put_contents($this->logfile, 'Got resumptionToken: ' . $node->item(0)->nodeValue . "\n", FILE_APPEND);            
                $arrRet['resumptionToken'] = $node->item(0)->nodeValue;
            } else {
                $arrRet['resumptionToken'] = false;
            }

            // get oai ids
            $idList = $dom->getElementsByTagname('identifier');
            if ($idList->length) {
                $arrRet['ids'] = array();
                foreach ($idList as $id) {
                    $arrRet['ids'][] = trim($id->nodeValue);
                }
                file_put_contents($this->logfile, 'Got: ' . count($arrRet['ids']) . ' oai ids' . "\n", FILE_APPEND);                        
            }
            return $arrRet;
        } else {
            return false;
        }
    }

    /**
     * Get oai url
     *
     * @return array oai ids
     */
    protected function getOaiUrl($collectionid) {
        $res = $GLOBALS['TYPO3_DB']->exec_SELECTquery('*', 'tx_jkzvdd_oai', 'collectionid="' . mysql_real_escape_string($collectionid) . '" and not hidden and not deleted');
        if ($GLOBALS['TYPO3_DB']->sql_num_rows($res)) {
            $arr = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res);
            $arrTmp = explode('?',$arr['oaiurl']);
            return trim($arrTmp[0]);
        } else {
            return false;
        }
    }

        /**
     * 
     * @author Jochen Kothe <kothe@sub.uni-goettingen.de>
     * @todo
     * @param
     * @return        
     */
    protected function id2name($id) {
        return str_replace('/', '___', trim($id));
    }

}

if (defined('TYPO3_MODE') && $GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/jkzvdd/tasks/class.tx_jkzvdd_oaigetmissing.php']) {
    include_once($GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/jktools/tasks/class.tx_jkzvdd_oaigetmissing.php']);
}
?>
