<?php

/*
 * ***************************************************************
 *   Copyright notice
 * 
 *   (c) 2013 Niedersächsische Staats- und Universitätsbibliothek Göttingen
 *   Jochen Kothe (kothe@sub.uni-goettingen.de, jk@profi-php.de)
 *   All rights reserved
 * 
 *   This script free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   The GNU General Public License can be found at
 *   http://www.gnu.org/copyleft/gpl.html.
 * 
 *   This script is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   This copyright notice MUST APPEAR in all copies of the script!
 * ***************************************************************
 */

class tx_jkzvdd_oaiharvest extends tx_scheduler_Task {

    var $extKey = 'jkzvdd';
    var $arrLang = array('default', 'en');

    public function execute() {
        $arrConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['jkzvdd']);

        // wenn (lastscan + scanperiod) kleiner als die aktuelle Zeit -> starte neues Harvesting
        $res = $GLOBALS['TYPO3_DB']->exec_SELECTquery('*', 'tx_jkzvdd_oai', '(lastscan + scanperiod)<' . time() . ' and not hidden and not deleted');
        if ($GLOBALS['TYPO3_DB']->sql_num_rows($res)) {
            while ($arr = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
                $arrURL = parse_url($arr['oaiurl']);
                parse_str($arrURL['query'], $arrQuery);

				$userStr = '';
				if ($arrURL['user']) {
					if ($arrURL['pass']) {
						$userStr = $arrURL['user'] . ':' . $arrURL['pass'] . '@';
					} else  {
						$userStr = $arrURL['user']  . '@';
					}
				}

				$portStr = '';
                		if ($arrURL['port']) {
					$portStr = ':' . $arrURL['port'];
				}

                //get "from" parameter with correct granularity
                $arrQuery['from'] = $this->getFrom($arrURL['scheme'] . '://' . $userStr . $arrURL['host'] . $portStr . $arrURL['path'] . '?verb=Identify', $arr['lastscan']);
                if($arrQuery['from'] === false) {
                    unset($arrQuery['from']);
                }

                $verb = $arrQuery['verb'];
                unset($arrQuery['verb']);

                $url = $arrURL['scheme'] . '://' . $userStr . $arrURL['host'] . ':' . $arrURL['port'] . $arrURL['path'];

                $command = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($this->extKey) . '/lib/oai_harvest.php ' . $arrConf['zvddrepository'] . ' ' . $arr['collectionid'] . ' ' . $url . ' ' . $verb;
                foreach ($arrQuery as $key => $val) {
                    $command .= ' ' . $key . '=' . $val;
                }

                $command .= ' >/dev/null 2>&1 &';

                $now = time();
                exec($command, $arrReturn, $status);

                //update 
                $GLOBALS['TYPO3_DB']->exec_UPDATEquery('tx_jkzvdd_oai', 'uid="' . $arr['uid'] . '"', array('lastscan' => $now));

                $LL = new DOMDocument('1.0', 'UTF-8');
                $test = $LL->load($arrConf['goobit3locallangfile']);
                if ($test) {
                    $LLxpath = new DOMXpath($LL);
                    $newLineNode = $LL->createTextNode("\n");

                    //Collection ID to one Token
                    $collectionid = strtolower($this->toOneToken($arr['collectionid']));
                    if ($collectionid != $arr['collectionid']) {
                        $arr['collectionid'] = $collectionid;
                        $GLOBALS['TYPO3_DB']->exec_UPDATEquery('tx_jkzvdd_oai', 'uid="' . $arr['uid'] . '"', array('collectionid' => $collectionid));
                    }

                    // insert / update language labels for goobit3
                    $nodeList = $LLxpath->evaluate('/T3locallang/meta[@type="array"]/labelContext[@type="array"]/label[@index="dc_' . $arr['collectionid'] . '"]');
                    // insert label
                    if (!$nodeList->length) {
                        // label
                        $nodeList = $LLxpath->evaluate('/T3locallang/meta[@type="array"]/labelContext[@type="array"]');
                        if ($nodeList->length) {
                            $nodeList->item(0)->appendChild($LL->createComment('created by jkzvdd on ' . date('Y-m-d H:i:s', time())));
                            $nodeList->item(0)->appendChild($newLineNode);
                            $label = $LL->createElement('label');
                            $label->setAttribute('index', 'dc_' . $arr['collectionid']);
                            $nodeList->item(0)->appendChild($label);
                            $nodeList->item(0)->appendChild($newLineNode);
                        }
                    }

                    foreach ($this->arrLang as $lang) {
                        $nodeList = $LLxpath->evaluate('/T3locallang/data[@type="array"]/languageKey[@index="' . $lang . '"][@type="array"]/label[@index="dc_' . $arr['collectionid'] . '"]');
                        // insert label
                        if (!$nodeList->length) {
                            $nodeList = $LLxpath->evaluate('/T3locallang/data[@type="array"]/languageKey[@index="' . $lang . '"][@type="array"]');
                            if ($nodeList->length) {
                                $nodeList->item(0)->appendChild($LL->createComment('created by jkzvdd on ' . date('Y-m-d H:i:s', time())));
                                $nodeList->item(0)->appendChild($newLineNode);
                                $label = $LL->createElement('label');
                                $label->setAttribute('index', 'dc_' . $arr['collectionid']);
                                $label->appendChild($LL->createTextNode($arr['collectionname']));
                                $nodeList->item(0)->appendChild($label);
                                $nodeList->item(0)->appendChild($newLineNode);
                            }
                            // update label
                        } else {
                            $nodeList->item(0)->nodeValue = '';
                            $nodeList->item(0)->appendChild($LL->createTextNode($arr['collectionname']));
                        }
                    }

                    $LL->save($arrConf['goobit3locallangfile']);
                }
            }
        }
        return true;
    }

    function getFrom($url, $lastscan) {
		### SSL Zeritikatsprüfung deaktivieren                 ###
		### stream context wir bei DomDocument->load() genutzt ###
		$arrOpts = array(
			'ssl' => array(
				'verify_peer' => false,
				'verify_peer_name' => false,
			)
		);
		$context = stream_context_create($arrOpts);
		libxml_set_streams_context($context);
		###########################################################

        $oai = new DOMDocument();
        $test = $oai->load($url);
        if ($test) {
            $nodeList = $oai->getElementsByTagName('granularity');
            if ($nodeList->length) {
                if (strlen(trim($nodeList->item(0)->nodeValue)) > 10) {
                    unset($oai);
                    return date('Y-m-d', $lastscan) . 'T' . date('H:i:s', $lastscan) . 'Z';
                } else {
                    unset($oai);
                    return date('Y-m-d', $lastscan);
                }
            } else {
                unset($oai);
                return false;
            }
        }
    }

    function toOneToken($str) {
        $str = trim($str);
        // first replace double spaces
        while (strpos($str, '  ') !== false) {
            $str = str_replace('  ', ' ', $str);
        }
        //replace hyphen, spaces... with points
        $str = str_replace(array(
            '&',
            '=',
            '+',
            '<',
            '>',
            '"',
            "'",
            '`',
            '_',
            '-',
            ' ',
            '/',
            ':',
            '\/',
            '\\'
                ), array(
            '.',
            '.',
            '.',
            '.',
            '.',
            '.',
            '.',
            '.',
            '.',
            '.',
            '.',
            '.',
            '.'
                ), $str);

        //replace double points
        while (strpos($str, '..') !== false) {
            $str = str_replace('..', '.', $str);
        }
        return $str;
    }

}

if (defined('TYPO3_MODE') && $GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/jkzvdd/tasks/class.tx_jkzvdd_oaiharvest.php']) {
    include_once($GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/jktools/tasks/class.tx_jkzvdd_oaiharvest.php']);
}
?>
