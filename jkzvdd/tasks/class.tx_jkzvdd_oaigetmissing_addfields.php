<?php

/*
 * ***************************************************************
 *   Copyright notice
 * 
 *   (c) 2013 Niedersächsische Staats- und Universitätsbibliothek Göttingen
 *   Jochen Kothe (kothe@sub.uni-goettingen.de, jk@profi-php.de)
 *   All rights reserved
 * 
 *   This script free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   The GNU General Public License can be found at
 *   http://www.gnu.org/copyleft/gpl.html.
 * 
 *   This script is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   This copyright notice MUST APPEAR in all copies of the script!
 * ***************************************************************
 */

class tx_jkzvdd_oaigetmissing_addFields implements tx_scheduler_AdditionalFieldProvider {

    public function __construct() {
         $this->arrConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['jkzvdd']);
 
         $arrExtList = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(',',$GLOBALS['TYPO3_CONF_VARS']['EXT']['extList']);
         if(in_array('goobit3',$arrExtList)) {
             $this->goobit3 = true;
         } else {
             $this->goobit3 = false;             
         }
    }
    /**
     * Add a multi select box with all available collections.
     *
     * @param array $taskInfo Reference to the array containing the info used in the add/edit form
     * @param object $task When editing, reference to the current task object. Null when adding.
     * @param tx_scheduler_Module $parentObject Reference to the calling object (Scheduler's BE module)
     * @return array Array containing all the information pertaining to the additional fields
     */
    public function getAdditionalFields(array &$taskInfo, $task, tx_scheduler_Module $parentObject) {
        // Initialize selected fields
        if (empty($taskInfo['selectedCollections'])) {
            if ($parentObject->CMD == 'edit') {
                $taskInfo['selectedCollections'] = $task->selectedCollections;
            } elseif ($parentObject->CMD == 'add') {
                $taskInfo['selectedCollections'] = array();
            }
        }

        $fieldName = 'tx_scheduler[selectedCollections][]';
        $fieldId = 'task_selectedCollections';
        $fieldOptions = $this->getCollectionOptions($taskInfo['selectedCollections']);
        $fieldHtml = '<select name="' . $fieldName . '" id="' . $fieldId . '" class="wide" size="30" multiple="multiple">' . $fieldOptions . '</select>';

        $additionalFields[$fieldId] = array(
            'code' => $fieldHtml,
            'label' => 'LLL:EXT:jkzvdd/locallang_tasks.xml:label.collections',
        );

        return $additionalFields;
    }

    /**
     * Checks that all selected collections exist in available collection list
     *
     * @param array $submittedData Reference to the array containing the data submitted by the user
     * @param tx_scheduler_Module $parentObject Reference to the calling object (Scheduler's BE module)
     * @return boolean TRUE if validation was ok (or selected class is not relevant), FALSE otherwise
     */
    public function validateAdditionalFields(array &$submittedData, tx_scheduler_Module $parentObject) {
        $validData = TRUE;

        $availableCollections = $this->getAvaibleCollections();
        if (is_array($submittedData['selectedCollections'])) {
            $invalidCollections = array_diff($submittedData['selectedCollections'], $availableCollections);
            if (!empty($invalidCollections)) {
                $parentObject->addMessage($GLOBALS['LANG']->sL('LLL:EXT:jkzvdd/locallang_tasks.xml:msg.selectionOfNonExistingCollections'), t3lib_FlashMessage::ERROR);
                $validData = FALSE;
            }
        } else {
            $parentObject->addMessage($GLOBALS['LANG']->sL('LLL:EXT:jkzvdd/locallang_tasks.xml:msg.noCollectionSelected'), t3lib_FlashMessage::ERROR);
            $validData = FALSE;
        }

        return $validData;
    }

    /**
     * Save selected collections in task object
     *
     * @param array $submittedData Contains data submitted by the user
     * @param tx_scheduler_Task $task Reference to the current task object
     * @return void
     */
    public function saveAdditionalFields(array $submittedData, tx_scheduler_Task $task) {
        $task->selectedCollections = $submittedData['selectedCollections'];
    }

    /**
     * Build select options of available collectionssand set currently selected collections
     *
     * @param array $selectedCollections Selected collections
     * @return string HTML of selectbox options
     */
    protected function getCollectionOptions(array $selectedCollections) {
        $options = array();

        $availableCollections = $this->getAvaibleCollections();
        $_availableCollections = array();
        if($this->goobit3) {            
            foreach($availableCollections as $key=>$collectionID) {
                $_availableCollections[$collectionID] = $GLOBALS['LANG']->sL('LLL:EXT:goobit3/locallang.xml:dc_'.trim($collectionID), $collectionID);
            }
        } else {
            $_availableCollections[$collectionID] = $collectionID; 
        }
        asort($_availableCollections);
        
        foreach ($_availableCollections as $collectionID=>$collectionName) {
            if (in_array($collectionID, $selectedCollections)) {
                $selected = ' selected="selected"';
            } else { 
                $selected = '';
            }
            $options[] = '<option value="' . $collectionID . '"' . $selected . '>' . $collectionName . '</option>';
        }

        return implode('', $options);
    }

    /**
     * Get all avaible collections
     *
     * @return array Registered collections
     */
    protected function getAvaibleCollections() {
        //get collections from solr
        $solrCollections = array();
        $solr = file_get_contents($this->arrConf['solrPhpsUrl'] . '&q=ismets:1&rows=0&facet=on&facet.field=dc');
        $arrSolr = unserialize($solr);
        if (is_array($arrSolr['facet_counts']['facet_fields']['dc'])) {
            foreach ($arrSolr['facet_counts']['facet_fields']['dc'] as $collection => $count) {
                $solrCollections[] = $collection;
            }
        }
        
        // get collections from oai provider
        $oaiCollections = array();
        $res = $GLOBALS['TYPO3_DB']->exec_SELECTquery('*','tx_jkzvdd_oai','not hidden and not deleted');
        if($GLOBALS['TYPO3_DB']->sql_num_rows($res)) {
            while($arr = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
                $oaiCollections[] = $arr['collectionid'];
            }
        }
        
        return array_intersect($solrCollections, $oaiCollections);
    }

}

?>
