<?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');
$TCA["tx_jkzvdd_mets"] = array (
	"ctrl" => array (
		'title'     => 'LLL:EXT:jkzvdd/locallang_db.xml:tx_jkzvdd_mets',		
		'label'     => 'collectionid',	
		'tstamp'    => 'tstamp',
		'crdate'    => 'crdate',
		'cruser_id' => 'cruser_id',
		'default_sortby' => "ORDER BY crdate",	
		'delete' => 'deleted',	
		'enablecolumns' => array (		
			'disabled' => 'hidden',
		),
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY).'tca.php',
		'iconfile'          => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY).'icon_tx_jkzvdd_mets.gif',
	),
	"feInterface" => array (
		"fe_admin_fieldList" => "hidden",
	)
);

$TCA["tx_jkzvdd_oai"] = array (
    "ctrl" => array (
        'title'     => 'LLL:EXT:jkzvdd/locallang_db.xml:tx_jkzvdd_oai',        
        'label'     => 'collectionid',    
        'tstamp'    => 'tstamp',
        'crdate'    => 'crdate',
        'cruser_id' => 'cruser_id',
        'default_sortby' => "ORDER BY crdate",    
        'delete' => 'deleted',    
        'enablecolumns' => array (        
            'disabled' => 'hidden',
        ),
        'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY).'tca.php',
        'iconfile'          => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY).'icon_tx_jkzvdd_oai.gif',
    ),
    "feInterface" => array (
        "fe_admin_fieldList" => "hidden",
    )
);

$TCA['be_users']['ctrl']['label'] = 'email';


if (TYPO3_MODE == 'BE')	{
		
//	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addModule('txjkzvddMmain','','',\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY).'modmain/');
}


if (TYPO3_MODE == 'BE')	{
		
//	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addModule('txjkzvddMmain','txjkzvddMoai','',\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY).'modoai/');
}


if (TYPO3_MODE == 'BE')	{
		
//	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addModule('txjkzvddMmain','txjkzvddMmets','',\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY).'modmets/');
}


\TYPO3\CMS\Core\Utility\GeneralUtility::loadTCA('tt_content');
$TCA['tt_content']['types']['list']['subtypes_excludelist'][$_EXTKEY.'_pioai']='layout,select_key,pages,recursive';
$TCA['tt_content']['types']['list']['subtypes_addlist'][$_EXTKEY.'_pioai']='pi_flexform';

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(array('LLL:EXT:jkzvdd/locallang_db.xml:tt_content.list_type_pioai', $_EXTKEY.'_pioai'),'list_type');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($_EXTKEY.'_pioai', 'FILE:EXT:'.$_EXTKEY.'/pioai/flexform_pioai.xml');

if (TYPO3_MODE=="BE")    $TBE_MODULES_EXT["xMOD_db_new_content_el"]["addElClasses"]["tx_jkzvdd_pioai_wizicon"] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY).'pioai/class.tx_jkzvdd_pioai_wizicon.php';


\TYPO3\CMS\Core\Utility\GeneralUtility::loadTCA('tt_content');
$TCA['tt_content']['types']['list']['subtypes_excludelist'][$_EXTKEY.'_pimets']='layout,select_key,pages,recursive';
#$TCA['tt_content']['types']['list']['subtypes_addlist'][$_EXTKEY.'_pimets']='pi_flexform';

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(array('LLL:EXT:jkzvdd/locallang_db.xml:tt_content.list_type_pimets', $_EXTKEY.'_pimets'),'list_type');
#\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($_EXTKEY.'_pimets', 'FILE:EXT:'.$_EXTKEY.'/pimets/flexform_pimets.xml');

if (TYPO3_MODE=="BE")    $TBE_MODULES_EXT["xMOD_db_new_content_el"]["addElClasses"]["tx_jkzvdd_pimets_wizicon"] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY).'pimets/class.tx_jkzvdd_pimets_wizicon.php';

?>
