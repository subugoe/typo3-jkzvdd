#!/usr/bin/env php
<?php

set_time_limit(0);
ini_set('memory_limit', '512M');

// Error-Meldungen abschalten
//error_reporting(E_ALL);

$repeat = 10;
$arrRecordUrls = array();

/* #######################################################################################################################################
  <key>=<val> = Uebergibt weitere URL-Parameter
  <verb>  = ListIdentifier: Es werden alle XML-Dateien einzeln anhand Ihres Identifiers geholt (besser bei langsamen Schnittstellen
  <verb>  = ListRecords: Die XML-Dateien werden direkt aus der Antwort extrahiert.

  nötigenfalss werden Anfragen bis zu "$repeat" mal wiederholt.

  must be called with full path, to get TYPO3 stuff
  Aufruf : /srv/www/chroot/zvdd/zvdd/htdocs/typo3conf/ext/jkzvdd/lib/oai_harvest.php <ZVDD Repository> <ZVDD Collection> <OAI-URL> <verb> <key>=<val>';
  Example:
  /srv/www/chroot/zvdd/zvdd/htdocs/typo3conf/ext/jkzvdd/lib/oai_harvest.php /srv/www/chroot/zvdd/zvdd/zvdd_repository autobiographica.sub.uni.goettingen.de http://gdz.sub.uni-goettingen.de/oai2/ ListRecords metadataPrefix=mets set=autobiographica

  ######################################################################################################################################### */

$strArgs = json_encode($_SERVER["argv"]);

$arrVerbs = array('ListRecords', 'ListIdentifiers');

$arrArgs['script'] = array_shift($_SERVER["argv"]);
$zvddRepository = array_shift($_SERVER["argv"]);
$zvddCol = array_shift($_SERVER["argv"]);
$test = @mkdir($zvddRepository . '/oai_provider/' . $zvddCol . '/logs/', 0775, true);
$logFile = $zvddRepository . '/oai_provider/' . $zvddCol . '/logs' . '/oai.log';

$arrArgs['oaiURL'] = array_shift($_SERVER["argv"]);
$arrArgs['verb'] = array_shift($_SERVER["argv"]);


foreach ($_SERVER["argv"] as $val) {
    $arrTmp = explode('=', $val);
    if(trim($arrTmp[1])) {
        $arrArgs2[$arrTmp[0]] = $arrTmp[1];
    }
}


file_put_contents($logFile, date('Y-m-d H:i:s', time()) . "\n", FILE_APPEND);
file_put_contents($logFile, 'Call : ' . $strArgs . "\n", FILE_APPEND);
file_put_contents($logFile, 'Start harvesting: ' . $arrArgs['oaiURL'] . "\n", FILE_APPEND);

if (!isset($arrArgs['oaiURL'])) {
    file_put_contents($logFile, 'no URL!' . "\n", FILE_APPEND);
    file_put_contents($logFile, 'Stop harvesting: ' . $arrArgs['oaiURL'] . "\n", FILE_APPEND);
    file_put_contents($logFile, date('Y-m-d H:i:s', time()) . "\n", FILE_APPEND);
    exit();
}


$oaiFolder = $zvddRepository . '/oai_provider/' . $zvddCol . '/oai/';
@mkdir($oaiFolder, 0775, true);

if (!is_dir($oaiFolder)) {
    file_put_contents($logFile, 'no OAI targetDir!' . "\n", FILE_APPEND);
    file_put_contents($logFile, 'Stop harvesting: ' . $arrArgs['oaiURL'] . "\n", FILE_APPEND);
    file_put_contents($logFile, date('Y-m-d H:i:s', time()) . "\n", FILE_APPEND);
    exit();
} else {
    if (!opendir($oaiFolder)) {
        file_put_contents($logFile, $oaiFolder . '" is not readable!' . "\n", FILE_APPEND);
        file_put_contents($logFile, 'Stop harvesting: ' . $arrArgs['oaiURL'] . "\n", FILE_APPEND);
        file_put_contents($logFile, date('Y-m-d H:i:s', time()) . "\n", FILE_APPEND);
        exit();
    }
    if (!touch($oaiFolder . '/tmp')) {
        file_put_contents($logFile, $oaiFolder . '" is not writeable!' . "\n", FILE_APPEND);
        file_put_contents($logFile, 'Stop harvesting: ' . $arrArgs['oaiURL'] . "\n", FILE_APPEND);
        file_put_contents($logFile, date('Y-m-d H:i:s', time()) . "\n", FILE_APPEND);
        exit();
    } else {
        unlink($oaiFolder . '/tmp');
    }
}

if (!isset($arrArgs['verb'])) {
    file_put_contents($logFile, ' no verb!' . "\n", FILE_APPEND);
    file_put_contents($logFile, 'Stop harvesting: ' . $arrArgs['oaiURL'] . "\n", FILE_APPEND);
    file_put_contents($logFile, date('Y-m-d H:i:s', time()) . "\n", FILE_APPEND);
    exit();
} else if (!in_array($arrArgs['verb'], $arrVerbs)) {
    file_put_contents($logFile, 'wrong verb!' . "\n", FILE_APPEND);
    file_put_contents($logFile, 'Stop harvesting: ' . $arrArgs['oaiURL'] . "\n", FILE_APPEND);
    file_put_contents($logFile, date('Y-m-d H:i:s', time()) . "\n", FILE_APPEND);
    exit();
}
if (!isset($arrArgs2['resumptionToken']) && !isset($arrArgs2['metadataPrefix'])) {
    file_put_contents($logFile, 'no metadataPrefix!' . "\n", FILE_APPEND);
    file_put_contents($logFile, 'Stop harvesting: ' . $arrArgs['oaiURL'] . "\n", FILE_APPEND);
    file_put_contents($logFile, date('Y-m-d H:i:s', time()) . "\n", FILE_APPEND);
    exit();
}

getXML($arrArgs, $arrArgs2);

$countRecords = count($arrRecordUrls);
$count = 0;
if ($countRecords) {
    //save Identifiers and URLs
    file_put_contents($logFile . '_IDs_' . date('YmdHis', time()), serialize($arrRecordUrls) . "\n");
    file_put_contents($logFile, date('Y-m-d H:i:s', time()) . "\n", FILE_APPEND);
    file_put_contents($logFile, 'found ' . $countRecords . ' Records.' . "\n", FILE_APPEND);
    foreach ($arrRecordUrls as $id => $recordUrl) {
        if (!($count % 5)) {
            file_put_contents($logFile, date('Y-m-d H:i:s', time()) . " - ", FILE_APPEND);
            file_put_contents($logFile, 'Got ' . $count . ' Records.' . "\n", FILE_APPEND);
        }
        getRecord($id, $recordUrl);
        $count++;
    }
    file_put_contents($logFile, date('Y-m-d H:i:s', time()) . " - ", FILE_APPEND);
    file_put_contents($logFile, 'Got ' . $count . ' Records.' . "\n", FILE_APPEND);
}
$arrRecordUrls = array();

file_put_contents($logFile, 'Stop harvesting: ' . $arrArgs['oaiURL'] . "\n", FILE_APPEND);
file_put_contents($logFile, date('Y-m-d H:i:s', time()) . "\n", FILE_APPEND);

function getXML($args, $args2) {
    global $repeat;
    global $logFile;
    global $oaiFolder;
    global $arrRecordUrls;

    $time_start = microtime(true);

    if (!array_key_exists('resumptionToken', $args2)) {
        $url = $args['oaiURL'] . '?verb=' . $args['verb'];
        foreach ($args2 as $key => $val) {
            $url .= '&' . $key . '=' . $val;
        }
    } else {
        if (!array_key_exists('metadataPrefix', $args2))
            $args2['metadataPrefix'] = 'mets';
//		$url = $args['oaiURL'].'?verb='.$args['verb'].'&resumptionToken='.$args2['resumptionToken'].'&metadataPrefix='.$args2['metadataPrefix'];
        $url = $args['oaiURL'] . '?verb=' . $args['verb'] . '&resumptionToken=' . urlencode($args2['resumptionToken']);
    }

    $count = 0;
    $xml = '';
    while (!$xml) {
        if ($count > $repeat) {
            file_put_contents($logFile, 'Give up!' . "\n", FILE_APPEND);
            file_put_contents($logFile, $url . "\n", FILE_APPEND);
            break;
        }
        $count++;
        file_put_contents($logFile, $count . '. Versuch: ' . $url . "\n", FILE_APPEND);
        $opts = array(
            'http' => array(
                'method' => "GET",
                'header' => "user-agent: ZVDD OAI2 Harvest\r\n",
                'follow_location' => 1,
            ),
			### SSL Zeritikatsprüfung deaktivieren                 ###
			'ssl' => array(
				'verify_peer' => false,
				'verify_peer_name' => false,
			)
        );
        $context = stream_context_create($opts);
        $xml = file_get_contents(trim($url), false, $context);
        if (!$xml) {
            sleep($count * 3);
        }
    }

    //open xml
    $dom = new DOMDocument('1.0', 'UTF-8');
    $dom->loadXML($xml);

    //get error
    $errorNode = $dom->getElementsByTagName('error');
    if ($errorNode->length) {
        $error = $errorNode->item(0)->nodeValue;
        if ($error) {
            file_put_contents($logFile, 'ERROR: ' . $error . "\n", FILE_APPEND);
            file_put_contents($logFile, 'Stop harvesting: ' . $args['oaiURL'] . "\n", FILE_APPEND);
            file_put_contents($logFile, date('Y-m-d H:i:s', time()) . "\n", FILE_APPEND);
            //exit();
            return false;
        }
    }

    unset($args2);

    //get resumptionToken
    $node = $dom->getElementsByTagName('resumptionToken');
    if ($node->length && trim($node->item(0)->nodeValue)) {
        $args2['resumptionToken'] = $node->item(0)->nodeValue;
        if ($args2['resumptionToken']) {
            //auf Platte schreiben
            writeXML($dom);
            file_put_contents($logFile, date('Y-m-d H:i:s', time()) . " - ", FILE_APPEND);
            if ($args['verb'] == 'ListIdentifiers') {
                file_put_contents($logFile, 'Got ' . count($arrRecordUrls) . ' Identifiers.' . "\n", FILE_APPEND);
            }
            getXML($args, $args2);
        }
    } else {
        //auf Platte schreiben
        writeXML($dom);
        file_put_contents($logFile, date('Y-m-d H:i:s', time()) . " - ", FILE_APPEND);
        if ($args['verb'] == 'ListIdentifiers') {
            file_put_contents($logFile, 'Got ' . count($arrRecordUrls) . ' Identifiers.' . "\n", FILE_APPEND);
        }
    }
}

function writeXML($dom) {
    global $arrArgs;
    global $arrArgs2;
    global $logFile;
    global $oaiFolder;
    global $arrRecordUrls;

    if ($arrArgs['verb'] == 'ListRecords') {
        $recordList = $dom->getElementsByTagname('record');
        if ($recordList->length) {
            file_put_contents($logFile, 'Write ListRecords Result' . "\n", FILE_APPEND);
            $dom->save($oaiFolder . '/' . uniqid() . '.xml');
        }
    } else if ($arrArgs['verb'] == 'ListIdentifiers') {
        $idList = $dom->getElementsByTagname('identifier');
        if ($idList->length) {
            foreach ($idList as $id) {
                // Nur RecordUrls zwischenspeichern nichts holen
                $arrRecordUrls[trim($id->nodeValue)] = $arrArgs['oaiURL'] . '?verb=GetRecord&identifier=' . trim($id->nodeValue) . '&metadataPrefix=' . $arrArgs2['metadataPrefix'];
            }
        }
    }
    return;
}

function getRecord($id, $url) {
    global $repeat;
    global $logFile;
    global $oaiFolder;

    $count = 0;
    $recordxml = '';
    $break = false;
    while (!$recordxml) {
        if ($count > $repeat) {
            file_put_contents($logFile, 'Give up!' . "\n", FILE_APPEND);
            file_put_contents($logFile, trim($id) . "\n", FILE_APPEND);
            return false;
        }
        $count++;
        file_put_contents($logFile, $count . '. Versuch: ' . trim($id) . "\n", FILE_APPEND);
        $opts = array(
            'http' => array(
                'method' => "GET",
                'header' => "user-agent: ZVDD OAI2 Harvest\r\n",
                'follow_location' => 1,
            ),
			### SSL Zeritikatsprüfung deaktivieren                 ###
			'ssl' => array(
				'verify_peer' => false,
				'verify_peer_name' => false,
			)
        );
        $context = stream_context_create($opts);
        $recordxml = file_get_contents(trim($url), false, $context);
    }

    $record = new DOMDocument('1.0', 'UTF-8');
    $test = $record->loadXML($recordxml);
    if ($test) {
        file_put_contents($logFile, 'Got record ' . $id . "\n", FILE_APPEND);
        $record->save($oaiFolder . str_replace('/', '___', trim($id) . '.xml'));
    } else {
        file_put_contents($logFile, 'ERROR: Cannot get record ' . $id . "\n", FILE_APPEND);
    }
}
?>
