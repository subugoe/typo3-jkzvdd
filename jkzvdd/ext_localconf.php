<?php
/* 
 * ***************************************************************
 *   Copyright notice
 * 
 *   (c) 2013 Niedersächsische Staats- und Universitätsbibliothek Göttingen
 *   Jochen Kothe (kothe@sub.uni-goettingen.de, jk@profi-php.de)
 *   All rights reserved
 * 
 *   This script free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   The GNU General Public License can be found at
 *   http://www.gnu.org/copyleft/gpl.html.
 * 
 *   This script is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   This copyright notice MUST APPEAR in all copies of the script!
 * ***************************************************************
 */

if (!defined('TYPO3_MODE')) die('Access denied.');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig('options.saveDocNew.tx_jkzvdd_mets=1');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig('options.saveDocNew.tx_jkzvdd_oai=1');

## Extending TypoScript from static template uid=43 to set up userdefined tag:
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript($_EXTKEY, 'editorcfg', 'tt_content.CSS_editor.ch.tx_jkzvdd_pioai = < plugin.tx_jkzvdd_pioai.CSS_editor', 43);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPItoST43($_EXTKEY, 'pioai/class.tx_jkzvdd_pioai.php', '_pioai', 'list_type', 0);


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript($_EXTKEY, 'editorcfg', 'tt_content.CSS_editor.ch.tx_jkzvdd_pimets = < plugin.tx_jkzvdd_pimets.CSS_editor', 43);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPItoST43($_EXTKEY, 'pimets/class.tx_jkzvdd_pimets.php', '_pimets', 'list_type', 0);

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['tx_jkzvdd_oaiharvest'] = array(
    'extension'        => $_EXTKEY,
    'title'            => 'OAI Harvester',
    'description'      => 'OAI Harvester'
);

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['tx_jkzvdd_oaigetmissing'] = array(
    'extension'        => $_EXTKEY,
    'title'            => 'Get Missing works by OAI-ID',
    'description'      => 'Get Missing works by OAI-ID',
    'additionalFields' => 'tx_jkzvdd_oaigetmissing_addFields'
);

?>
