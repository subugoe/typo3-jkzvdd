<?php
/* 
 * ***************************************************************
 *   Copyright notice
 * 
 *   (c) 2013 Niedersächsische Staats- und Universitätsbibliothek Göttingen
 *   Jochen Kothe (kothe@sub.uni-goettingen.de, jk@profi-php.de)
 *   All rights reserved
 * 
 *   This script free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   The GNU General Public License can be found at
 *   http://www.gnu.org/copyleft/gpl.html.
 * 
 *   This script is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   This copyright notice MUST APPEAR in all copies of the script!
 * ***************************************************************
 */

$extensionPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('jkzvdd');
return array(
    'tx_jkzvdd_oaiharvest' => $extensionPath . 'tasks/class.tx_jkzvdd_oaiharvest.php',
    'tx_jkzvdd_oaigetmissing' => $extensionPath . 'tasks/class.tx_jkzvdd_oaigetmissing.php',
    'tx_jkzvdd_oaigetmissing_addfields' => $extensionPath . 'tasks/class.tx_jkzvdd_oaigetmissing_addfields.php',
);
?>
