CREATE TABLE tx_jkzvdd_mets (
    uid int(11) NOT NULL auto_increment,
    pid int(11) DEFAULT '0' NOT NULL,
    tstamp int(11) DEFAULT '0' NOT NULL,
    crdate int(11) DEFAULT '0' NOT NULL,
    cruser_id int(11) DEFAULT '0' NOT NULL,
    deleted tinyint(4) DEFAULT '0' NOT NULL,
    hidden tinyint(4) DEFAULT '0' NOT NULL,
    metsfile blob NOT NULL,
    collectionid tinytext NOT NULL,
    collectionname tinytext NOT NULL,
    uploaddate int(11) DEFAULT '0' NOT NULL,
    emailuploader tinytext NOT NULL,
    
    PRIMARY KEY (uid),
    KEY parent (pid)
);


#
# Table structure for table 'tx_jkzvdd_oai'
#
CREATE TABLE tx_jkzvdd_oai (
    uid int(11) NOT NULL auto_increment,
    pid int(11) DEFAULT '0' NOT NULL,
    tstamp int(11) DEFAULT '0' NOT NULL,
    crdate int(11) DEFAULT '0' NOT NULL,
    cruser_id int(11) DEFAULT '0' NOT NULL,
    deleted tinyint(4) DEFAULT '0' NOT NULL,
    hidden tinyint(4) DEFAULT '0' NOT NULL,
    oaiurl tinytext NOT NULL,
    collectionid tinytext NOT NULL,
    collectionname tinytext NOT NULL,
    entrydate int(11) DEFAULT '0' NOT NULL,
    emailentry tinytext NOT NULL,
    lastscan int(11) DEFAULT '0' NOT NULL,
    scanperiod int(11) unsigned DEFAULT '0' NOT NULL,

    PRIMARY KEY (uid),
    KEY parent (pid)
);
