<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "jkzvdd".
 *
 * Auto generated 23-06-2013 12:43
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
	'title' => 'ZVDD Suite',
	'description' => 'METS OAI2 Harvester und Uplod Suite',
	'category' => 'misc',
	'author' => 'Jochen Kothe',
	'author_email' => 'jk@profi-php.de',
	'shy' => '',
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'module' => 'modmain,modoai,modmets',
	'state' => 'alpha',
	'internal' => '',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author_company' => '',
	'version' => '0.0.0',
	'constraints' => array(
		'depends' => array(
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:46:{s:9:"ChangeLog";s:4:"6cb8";s:21:"ext_conf_template.txt";s:4:"f40a";s:12:"ext_icon.gif";s:4:"c054";s:17:"ext_localconf.php";s:4:"9b47";s:14:"ext_tables.php";s:4:"5b78";s:14:"ext_tables.sql";s:4:"dbd5";s:28:"ext_typoscript_constants.txt";s:4:"4460";s:28:"ext_typoscript_editorcfg.txt";s:4:"3f6d";s:24:"ext_typoscript_setup.txt";s:4:"4bea";s:23:"icon_tx_jkzvdd_mets.gif";s:4:"475a";s:22:"icon_tx_jkzvdd_oai.gif";s:4:"475a";s:16:"locallang_db.xml";s:4:"cad3";s:10:"README.txt";s:4:"ee2d";s:7:"tca.php";s:4:"b618";s:12:"cli/conf.php";s:4:"976f";s:15:"cli/cronjob.php";s:4:"386f";s:24:"cli/cronjob_20130623.php";s:4:"d5ad";s:19:"cli/oai_harvest.php";s:4:"7c1a";s:28:"cli/oai_harvest_20130623.php";s:4:"85fd";s:17:"modmain/clear.gif";s:4:"cc11";s:16:"modmain/conf.php";s:4:"c49a";s:25:"modmain/locallang_mod.xml";s:4:"918d";s:22:"modmain/moduleicon.gif";s:4:"e7e0";s:22:"modmain/moduleicon.png";s:4:"f819";s:17:"modmets/clear.gif";s:4:"cc11";s:16:"modmets/conf.php";s:4:"1969";s:17:"modmets/index.php";s:4:"e22e";s:21:"modmets/locallang.xml";s:4:"b84f";s:25:"modmets/locallang_mod.xml";s:4:"5dab";s:22:"modmets/moduleicon.gif";s:4:"e7e0";s:16:"modoai/clear.gif";s:4:"cc11";s:15:"modoai/conf.php";s:4:"8662";s:16:"modoai/index.php";s:4:"81c6";s:20:"modoai/locallang.xml";s:4:"e302";s:24:"modoai/locallang_mod.xml";s:4:"8727";s:21:"modoai/moduleicon.gif";s:4:"e7e0";s:33:"pimets/class.tx_jkzvdd_pimets.php";s:4:"8920";s:41:"pimets/class.tx_jkzvdd_pimets_wizicon.php";s:4:"578f";s:16:"pimets/clear.gif";s:4:"cc11";s:20:"pimets/locallang.xml";s:4:"0c85";s:31:"pioai/class.tx_jkzvdd_pioai.php";s:4:"2099";s:39:"pioai/class.tx_jkzvdd_pioai_wizicon.php";s:4:"0689";s:15:"pioai/clear.gif";s:4:"cc11";s:24:"pioai/flexform_pioai.xml";s:4:"6fb5";s:19:"pioai/locallang.xml";s:4:"edee";s:19:"res/pioai_tmpl.html";s:4:"6316";}',
	'suggests' => array(
	),
);

?>