<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2009 Jochen Kothe <jk@profi-php.de>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/


/**
 * Plugin 'OAI Provider Form' for the 'jkzvdd' extension.
 *
 * @author    Jochen Kothe <jk@profi-php.de>
 * @package    TYPO3
 * @subpackage    tx_jkzvdd
 */
class tx_jkzvdd_pioai extends \TYPO3\CMS\Frontend\Plugin\AbstractPlugin {
    var $prefixId      = 'tx_jkzvdd_pioai';        // Same as class name
    var $scriptRelPath = 'pioai/class.tx_jkzvdd_pioai.php';    // Path to this script relative to the extension dir.
    var $extKey        = 'jkzvdd';    // The extension key.
    var $pi_checkCHash = true;
    
    /**
     * The main method of the PlugIn
     *
     * @param    string        $content: The PlugIn content
     * @param    array        $conf: The PlugIn configuration
     * @return    The content that is displayed on the website
     */
    function main($content,$conf)    {
        $this->conf=$conf;
        $this->pi_setPiVarDefaults();
        $this->pi_loadLL();
        //cache page - no cache plugin content
        $cache = 1;
        $this->pi_USER_INT_obj = 1;

        //get template
        $this->tmpl = $this->cObj->fileResource($this->conf['templateFile']);

        // flexform xml to array
        $this->pi_initPIflexForm();

        if($this->cObj->data['pi_flexform']['data']) {
            foreach($this->cObj->data['pi_flexform']['data'] as $sheetname => $data) {
                foreach($data as $lang => $arrSheet ) {
                    foreach($arrSheet as $key => $value) {
                        $flexValue = $this->pi_getFFvalue($this->cObj->data['pi_flexform'], $key, $sheetname);
                        // Check for an existing value, or a zero value
                        if (!empty($flexValue) || $flexValue == '0') {
                            // Override TS setup
                            $this->conf[$key] = $flexValue;
                        }
                    }
                }
            }
        }
        
        //get GET and POST vars
        $this->arrGP = $this->getGP();

        //check oai form
        if($this->arrGP['submit']) {
            $errmsg = '';
            //check Values
            if(!trim($this->piVars['oaiurl'])) {
                $errmsg .= $this->pi_getLL('pioai_missingoaiurl','Keine OAI URL angegeben!').'<br/>';
            } else if(!$this->checkUrl(trim($this->piVars['oaiurl']))) {
                $errmsg .= $this->pi_getLL('pioai_incorrectemailentry','Die OAI URL ist nicht valide!').'<br/>';
            }
            if(!trim($this->piVars['collectionname'])) {
                $errmsg .= $this->pi_getLL('pioai_missingcollectionname','Der Sammlungsname fehlt!').'<br/>';
            }
            if(!trim($this->piVars['emailentry'])) {
                $errmsg .= $this->pi_getLL('pioai_missingemailentry','Emailadresse des Ansprechpartners fehlt!').'<br/>';
            } else if(!\TYPO3\CMS\Core\Utility\GeneralUtility::validEmail(trim($this->piVars['emailentry']))) {
                $errmsg .= $this->pi_getLL('pioai_incorrectemailentry','Die Emailadresse ist nicht valide!').'<br/>';
            }
            if(trim($errmsg)) {
                $content = $this->renderOAIForm($this->tmpl,$errmsg);
            } else {
                //check Entry
                $res = $GLOBALS['TYPO3_DB']->exec_SELECTquery('*','tx_jkzvdd_oai','oaiurl="'.trim($this->piVars['oaiurl']).'" and not deleted');
                if($GLOBALS['TYPO3_DB']->sql_num_rows($res)) {
                    $errmsg = $this->pi_getLL('pioai_collectionerror','Die Sammlung ist bereits angemeldet!').'<br/>';
                    $content = $this->renderOAIForm($this->tmpl,$errmsg);
                } else {
                    $arrUrl = parse_url($this->piVars['oaiurl']);
                    $arrQuery = array();
                    parse_str($arrUrl['query'],$arrQuery);
                    if(isset($arrQuery['set'])) {
                        $collectionid = $this->toOneToken($arrQuery['set'].'.'.str_replace('www.','',$arrUrl['host']));
                    } else {
                        $collectionid = $this->toOneToken(str_replace('www.','',$arrUrl['host']));
                    }
                    $stamp = time();
                    //Eintragen
                    $arrVal['pid'] = $this->conf['oaistoragepid'];
                    $arrVal['tstamp'] = $stamp;
                    $arrVal['crdate'] = $stamp;
                    $arrVal['hidden'] = '1';
                    $arrVal['oaiurl'] = $this->piVars['oaiurl'];
                    $arrVal['collectionid'] = $collectionid;
                    $arrVal['collectionname'] = $this->piVars['collectionname'];
                    $arrVal['entrydate'] = $stamp;
                    $arrVal['emailentry'] = $this->piVars['emailentry'];
                    $arrVal['lastscan'] = '1';
                    $arrVal['scanperiod'] = $this->piVars['scanperiod'];
                    $GLOBALS['TYPO3_DB']->exec_INSERTquery('tx_jkzvdd_oai',$arrVal);

                    //sent email
					$mail = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Mail\\MailMessage');
                    $resOAIAdmin = $GLOBALS['TYPO3_DB']->exec_SELECTquery('email, realName','be_users','uid in ('.$this->conf['oaiadmins'].')');
                    while($arrOAIAdmin =  $GLOBALS['TYPO3_DB']->sql_fetch_assoc($resOAIAdmin)) { 
                        $message = '####################################'."\n";
                        $message .= '### auto generated do not answer ###'."\n";
                        $message .= '####################################'."\n";
                        $message .= "\n";
                        $message .= 'Sammlung: '.$arrVal['collectionname']."\n";
                        $message .= 'OAI URL: <'.$arrVal['oaiurl'].'>'."\n";
                        $message .= 'Email: <'.$arrVal['emailentry'].'>'."\n";
                        $message .= "\n";

						$mail->setFrom(array('noreply@zvdd.de' => 'ZVDD Anmeldung'));
						$mail->setTo(array($arrOAIAdmin['email'] => $arrOAIAdmin['email']));
						$mail->setSubject('[ZVDD] OAI Anmeldung');
						$mail->setBody($message);
						$mail->send();
					}

                    $content = $this->cObj->getSubpart($this->tmpl, '###OAI_SUCCESS###');
                }
            }
        } else {

            $content = $this->renderOAIForm($this->tmpl);
        }


        return $this->pi_wrapInBaseClass($content);
    }

    /**
    * Returns html content for upload form
    *
    * @param    string      $errMsg: Fehlermeldung
    * @return   html content
    */
    function renderOAIForm($tmpl,$errMsg='') {
        $tmplForm = $this->cObj->getSubpart($tmpl, '###OAI_FORM###');
        $marker['###ERRORMSG###'] = $errMsg;
        $marker['###HIDDENFIELDS###'] = $this->appendHiddenFields(array());
        $marker['###LABEL_OAIURL###'] = $this->pi_getLL('pioai_oaiurl','OAI URL:');
        $marker['###OAIURL###'] = trim($this->piVars['oaiurl']);
        $marker['###LABEL_COLLECTIONNAME###'] = $this->pi_getLL('pioai_collectionname','Name der Sammlung:');
        $marker['###COLLECTIONNAME###'] = trim($this->piVars['collectionname']);
        $marker['###LABEL_EMAILENTRY###'] = $this->pi_getLL('pioai_emailentry','Email Adresse Ansprechpartner:');
        $marker['###EMAILENTRY###'] = trim($this->piVars['emailentry']);
        $marker['###LABEL_SCANPERIOD###'] = $this->pi_getLL('pioai_scanperiod','Wie häufig gibt es neue Inhalte?');
        $marker['###DAILY###'] = $this->pi_getLL('pioai_daily','täglich');
        $marker['###WEEKLY###'] = $this->pi_getLL('pioai_weekly','wöchenlich');
        $marker['###MONTHLY###'] =  $this->pi_getLL('pioai_monthly','monatlich');
        $marker['###D_CHECKED###'] = '';
        $marker['###W_CHECKED###'] = '';
        $marker['###M_CHECKED###'] = '';
        switch($this->piVars['scanperiod']) {
            case '86400':
                $marker['###D_CHECKED###'] = 'selected="selected"'; 
            break;
            case '604800':
                $marker['###W_CHECKED###'] = 'selected="selected"'; 
            break;
            case '2592000':
                $marker['###M_CHECKED###'] = 'selected="selected"'; 
            break;
            default;
                $marker['###D_CHECKED###'] = 'selected="selected"'; 
            break;
        }
        $marker['###EMAILENTRY###'] = trim($this->piVars['emailentry']);
        $marker['###SUBMIT###'] = $this->pi_getLL('pi_submit','anmelden');
        return $this->cObj->substituteMarkerArrayCached($tmplForm, $marker);
    }

    /**
    * Returns html content hidden form fields
    *
    * @param    array       $arr contains name=>value pairs for hidden fields
    * @param    string      $content fields will be appended to $content
    * @return   html content
    */
    function appendHiddenFields($arr,$content='') {
        foreach($arr as $key=>$val) {
            $content .= '<input type="hidden" name="'.$key.'" value="'.$val.'" />'."\n";
        }
        return $content;
    }

    /**
    * [Describe function...]
    *
    * @return [type]  ...
    */
    function getGP() {
        $arrGET = \TYPO3\CMS\Core\Utility\GeneralUtility::_GET();
        $arrPOST = \TYPO3\CMS\Core\Utility\GeneralUtility::_POST();
        return array_merge($arrGET, $arrPOST);
    }

    function checkUrl($url){
        return preg_match('%(http://|www){1}([\w-?&;,!#~=\./\@]+\.[\w/]+)%', $url);
    }
    function toOneToken($str) {
        $str = trim($str);
        // first replace double spaces
        while (strpos($str, '  ') !== false) {
            $str = str_replace('  ', ' ', $str);
        }
        //replace hyphen, spaces... with points
        $str = str_replace(array(
            '&',
            '=',
            '+',
            '<',
            '>',
            '"',
            "'",
            '`',
            '_',
            '-',
            ' ',
            '/',
            ':',
            '\\'
        ), array (
            '.',
            '.',
            '.',
            '.',
            '.',
            '.',
            '.',
            '.',
            '.',
            '.',
            '.',
            '.',
            '.'
        ), $str);

        //replace double points
        while (strpos($str, '..') !== false) {
            $str = str_replace('..', '.', $str);
        }
        return $str;
    }

}

if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/jkzvdd/pioai/class.tx_jkzvdd_pioai.php'])    {
    include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/jkzvdd/pioai/class.tx_jkzvdd_pioai.php']);
}

?>
